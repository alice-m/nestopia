#pragma once

#include <highscore/libhighscore.h>

G_BEGIN_DECLS

#define NESTOPIA_TYPE_CORE (nestopia_core_get_type())

G_DECLARE_FINAL_TYPE (NestopiaCore, nestopia_core, NESTOPIA, CORE, HsCore)

G_MODULE_EXPORT GType hs_get_core_type (void);

G_END_DECLS

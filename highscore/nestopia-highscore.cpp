#include "nestopia-highscore.h"

#include "../src/api/NstApiCartridge.hpp"
#include "../src/api/NstApiEmulator.hpp"
#include "../src/api/NstApiFds.hpp"
#include "../src/api/NstApiInput.hpp"
#include "../src/api/NstApiMachine.hpp"
#include "../src/api/NstApiSound.hpp"
#include "../src/api/NstApiUser.hpp"
#include "../src/api/NstApiVideo.hpp"

#include "../src/NstCore.hpp"
#include "../src/NstPatcher.hpp"

#include <cmath>
#include <fstream>
#include <sstream>

#define BYTES_PER_PIXEL 4
#define SAMPLE_RATE 48000
#define VERT_OVERSCAN 8
#define HORZ_OVERSCAN 0

struct _NestopiaCore
{
  HsCore parent_instance;

  Nes::Api::Emulator *emulator;
  Nes::Core::Video::Output *video;
  Nes::Core::Sound::Output *sound;
  Nes::Core::Input::Controllers *input;

  HsSoftwareContext *context;
  int16_t *audio_buffer;

  char *rom_location;
  char *save_location;

  char *save_data;
  size_t save_size;

  gboolean mic_loud;

  double paddle_value;
};

static void nestopia_nes_core_init (HsNesCoreInterface *iface);
static void nestopia_fds_core_init (HsFdsCoreInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (NestopiaCore, nestopia_core, HS_TYPE_CORE,
                               G_IMPLEMENT_INTERFACE (HS_TYPE_NES_CORE, nestopia_nes_core_init)
                               G_IMPLEMENT_INTERFACE (HS_TYPE_FDS_CORE, nestopia_fds_core_init));

static gboolean
is_pal (NestopiaCore *self)
{
  return Nes::Api::Machine (*self->emulator).GetMode () == Nes::Api::Machine::PAL;
}

static gboolean
is_fds (NestopiaCore *self)
{
  return hs_core_get_platform (HS_CORE (self)) == HS_PLATFORM_FAMICOM_DISK_SYSTEM;
}

static bool NST_CALLBACK
video_lock_cb (gpointer user_data, Nes::Core::Video::Output& video)
{
  NestopiaCore *self = NESTOPIA_CORE (user_data);

  video.pixels = hs_software_context_get_framebuffer (self->context);
  video.pitch = Nes::Core::Video::Output::WIDTH * BYTES_PER_PIXEL;

  return true;
}

static void NST_CALLBACK
video_unlock_cb (gpointer user_data, Nes::Core::Video::Output& video) {
}

static void NST_CALLBACK
file_io_cb (gpointer user_data, Nes::Api::User::File &file)
{
  NestopiaCore *self = NESTOPIA_CORE (user_data);
  g_autoptr (GFile) gfile = NULL;
  GError *error = NULL;
  char *data;
  gsize size;

  g_assert (self->save_location);

  switch(file.GetAction()) {
    case Nes::Api::User::File::LOAD_BATTERY:
    case Nes::Api::User::File::LOAD_EEPROM:
    case Nes::Api::User::File::LOAD_TAPE:
    case Nes::Api::User::File::LOAD_TURBOFILE:
      file.GetRawStorage ((gpointer&) self->save_data, self->save_size);

      gfile = g_file_new_for_path (self->save_location);
      if (g_file_query_exists (gfile, NULL)) {
        if (!g_file_load_contents (gfile, NULL, &data, &size, NULL, &error)) {
          hs_core_log (HS_CORE (self), HS_LOG_WARNING, "Couldn't load NES save data: %s", error->message);
          g_clear_error (&error);
        }

        file.SetContent (data, size);
      }

      break;

    case Nes::Api::User::File::LOAD_FDS:
      file.GetRawStorage ((gpointer&) self->save_data, self->save_size);

      gfile = g_file_new_for_path (self->save_location);

      if (g_file_query_exists (gfile, NULL)) {
        std::ifstream stream (self->save_location, std::ifstream::in | std::ifstream::binary);

        if (stream.is_open ())
          file.SetPatchContent (stream);
      }

      break;

    case Nes::Api::User::File::SAVE_BATTERY:
    case Nes::Api::User::File::SAVE_EEPROM:
    case Nes::Api::User::File::SAVE_TAPE:
    case Nes::Api::User::File::SAVE_TURBOFILE:
    case Nes::Api::User::File::SAVE_FDS:
      // Saving is done via hs_core_save_data() instead
      break;

    case Nes::Api::User::File::LOAD_ROM:
    case Nes::Api::User::File::LOAD_SAMPLE:
    case Nes::Api::User::File::LOAD_SAMPLE_MOERO_PRO_YAKYUU:
    case Nes::Api::User::File::LOAD_SAMPLE_MOERO_PRO_YAKYUU_88:
    case Nes::Api::User::File::LOAD_SAMPLE_MOERO_PRO_TENNIS:
    case Nes::Api::User::File::LOAD_SAMPLE_TERAO_NO_DOSUKOI_OOZUMOU:
    case Nes::Api::User::File::LOAD_SAMPLE_AEROBICS_STUDIO:
      break;
  }
}

static void NST_CALLBACK
log_cb (gpointer user_data, const char *str, gulong length)
{
  NestopiaCore *self = NESTOPIA_CORE (user_data);

  hs_core_log_literal (HS_CORE (self), HS_LOG_INFO, str);
}

static inline gboolean
load_rom (NestopiaCore  *self,
          const char    *rom_path,
          GError       **error)
{
  Nes::Api::Machine machine (*self->emulator);
  std::ifstream file (rom_path);

  Nes::Result result = machine.Load (file, Nes::Api::Machine::FAVORED_NES_NTSC);

  if (NES_FAILED (result)) {
    switch (result) {
    case Nes::RESULT_ERR_INVALID_FILE:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_COULDNT_LOAD_ROM, "Invalid file");
      break;
    case Nes::RESULT_ERR_OUT_OF_MEMORY:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_OUT_OF_MEMORY, "Out of Memory");
      break;
    case Nes::RESULT_ERR_CORRUPT_FILE:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_COULDNT_LOAD_ROM, "Corrupt or Missing File");
      break;
    case Nes::RESULT_ERR_UNSUPPORTED_MAPPER:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_UNSUPPORTED_GAME, "Unsupported Mapper");
      break;
    case Nes::RESULT_ERR_MISSING_BIOS:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_MISSING_BIOS, "Missing BIOS");
      break;
    default:
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Unknown error: %d", result);
      break;
    }

    return FALSE;
  }

  machine.SetMode (machine.GetDesiredMode ());

  machine.Power (true);

  return TRUE;
}

static gboolean
nestopia_core_load_rom (HsCore      *core,
                        const char **rom_paths,
                        int          n_rom_paths,
                        const char  *save_path,
                        GError     **error)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  g_assert (n_rom_paths == 1);

  g_set_str (&self->rom_location, rom_paths[0]);
  g_set_str (&self->save_location, save_path);

  // Load the cartridge database
  Nes::Api::Cartridge::Database database (*self->emulator);
  if (!database.IsLoaded ()) {
    std::ifstream database_file (CORE_DIR G_DIR_SEPARATOR_S "NstDatabase.xml",
                                 std::ifstream::in | std::ifstream::binary);

    if (!database_file.is_open()) {
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_IO, "Failed to open database");

      return FALSE;
    }

    Nes::Result result = database.Load (database_file);

    if (NES_FAILED (result)) {
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to load database");

      return FALSE;
    }

    database.Enable (true);
    database_file.close ();
  }

  // Load the game
  if (!load_rom (self, self->rom_location, error))
    return FALSE;

  // Set region and controllers from the database
  Nes::Api::Input input (*self->emulator);
  input.AutoSelectControllers ();
  input.AutoSelectAdapter ();

  if (is_fds (self)) {
    Nes::Api::Fds fds (*self->emulator);

    fds.InsertDisk (0, 0);
  }

  // Set up video
  self->context =
    hs_core_create_software_context (HS_CORE (self),
                                     Nes::Core::Video::Output::WIDTH,
                                     Nes::Core::Video::Output::HEIGHT,
                                     HS_PIXEL_FORMAT_B8G8R8X8);

  Nes::Api::Video video (*self->emulator);
  Nes::Api::Video::RenderState renderState;

  renderState.bits.count = 32;
  renderState.bits.mask.r = 0xFF0000;
  renderState.bits.mask.g = 0x00FF00;
  renderState.bits.mask.b = 0x0000FF;

  renderState.filter = Nes::Api::Video::RenderState::FILTER_NONE;
  renderState.width = Nes::Core::Video::Output::WIDTH;
  renderState.height = Nes::Core::Video::Output::HEIGHT;

  video.GetPalette ().SetMode (Nes::Api::Video::Palette::MODE_YUV);
  video.SetDecoder (Nes::Api::Video::DecoderPreset::DECODER_CONSUMER);

  if (NES_FAILED (video.SetRenderState (renderState))) {
    g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to set render state");

    return FALSE;
  }

  HsRectangle area = HS_RECTANGLE_INIT (HORZ_OVERSCAN, VERT_OVERSCAN,
                                        Nes::Core::Video::Output::WIDTH - HORZ_OVERSCAN * 2,
                                        Nes::Core::Video::Output::HEIGHT - VERT_OVERSCAN * 2);
  hs_software_context_set_area (self->context, &area);

  // Set up audio
  if (self->audio_buffer)
    delete self->audio_buffer;

  int length = SAMPLE_RATE / hs_core_get_frame_rate (core);
  self->audio_buffer = new int16_t[length];
  memset (self->audio_buffer, 0, length * sizeof (int16_t));

  self->sound->samples[0] = self->audio_buffer;
  self->sound->length[0] = length;
  self->sound->samples[1] = NULL;
  self->sound->length[1] = 0;

  Nes::Api::Sound sound (*self->emulator);
  sound.SetSampleRate (SAMPLE_RATE);
  sound.SetSpeaker (Nes::Api::Sound::SPEAKER_MONO);
  sound.SetSpeed (Nes::Api::Sound::DEFAULT_SPEED);
  sound.SetVolume (Nes::Api::Sound::ALL_CHANNELS, 100);

  return TRUE;
}

const int PAD_BUTTON_MAPPING[] = {
  Nes::Core::Input::Controllers::Pad::UP,
  Nes::Core::Input::Controllers::Pad::DOWN,
  Nes::Core::Input::Controllers::Pad::LEFT,
  Nes::Core::Input::Controllers::Pad::RIGHT,
  Nes::Core::Input::Controllers::Pad::A,
  Nes::Core::Input::Controllers::Pad::B,
  Nes::Core::Input::Controllers::Pad::SELECT,
  Nes::Core::Input::Controllers::Pad::START,
};

static inline int
transform_x (double x)
{
  return HORZ_OVERSCAN + x * (Nes::Core::Video::Output::WIDTH - HORZ_OVERSCAN * 2);
}

static inline int
transform_y (double y)
{
  return VERT_OVERSCAN + y * (Nes::Core::Video::Output::HEIGHT - VERT_OVERSCAN * 2);
}

static void
nestopia_core_poll_input (HsCore *core, HsInputState *input_state)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  for (int player = 0; player < HS_NES_MAX_PLAYERS; player++) {
    guint32 buttons = input_state->nes.pad_buttons[player];

    for (int btn = 0; btn < HS_NES_N_BUTTONS; btn++) {
      if (buttons & 1 << btn)
        self->input->pad[player].buttons |= PAD_BUTTON_MAPPING[btn];
      else
        self->input->pad[player].buttons &= ~PAD_BUTTON_MAPPING[btn];
    }
  }

  self->mic_loud = (input_state->nes.mic_level == HS_NES_MIC_LEVEL_LOUD);

  self->input->zapper.fire = input_state->nes.zapper_fire;
  self->input->zapper.x = transform_x (input_state->nes.zapper_x);
  self->input->zapper.y = transform_y (input_state->nes.zapper_y);

  double paddle_pos;
  if (std::isnan (input_state->nes.paddle_position)) {
    double fps = hs_core_get_frame_rate (HS_CORE (self));

    self->paddle_value = CLAMP (self->paddle_value + input_state->nes.paddle_speed / fps, -1, 1);

    paddle_pos = self->paddle_value;
  } else {
    paddle_pos = input_state->nes.paddle_position;
  }

  self->input->paddle.x = (paddle_pos * 60) + 106; // 46-166
  self->input->paddle.button = input_state->nes.paddle_button;
}

static void
nestopia_core_run_frame (HsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  self->emulator->Execute (self->video, self->sound, self->input);

  if (self->mic_loud)
    self->input->pad[1].mic ^= Nes::Api::Input::Controllers::Pad::MIC;
  else
    self->input->pad[1].mic = 0;

  int length = SAMPLE_RATE / hs_core_get_frame_rate (core);
  hs_core_play_samples (core, self->audio_buffer, length);
}

static void
nestopia_core_reset (HsCore *core, gboolean hard)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  Nes::Api::Machine (*self->emulator).Reset (hard);

  if (is_fds (self) && hard) {
    Nes::Api::Fds fds (*self->emulator);

    fds.EjectDisk ();
    fds.InsertDisk (0, 0);
  }
}

static void
nestopia_core_stop (HsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Machine machine (*self->emulator);

  machine.Power (false);
  machine.Unload ();

  if (self->audio_buffer) {
    delete self->audio_buffer;
    self->audio_buffer = NULL;
  }

  self->save_data = NULL;
  self->save_size = 0;

  g_clear_pointer (&self->rom_location, g_free);
  g_clear_pointer (&self->save_location, g_free);
  g_clear_object (&self->context);
}

static gboolean
nestopia_core_reload_save (HsCore      *core,
                           const char  *save_path,
                           GError     **error)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Machine machine (*self->emulator);

  Nes::Result result = machine.Unload ();

  if (NES_FAILED (result)) {
    g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to unload ROM");
    return FALSE;
  }

  g_set_str (&self->save_location, save_path);

  return load_rom (self, self->rom_location, error);
}

static gboolean
nestopia_core_sync_save (HsCore  *core,
                         GError **error)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  g_autoptr (GFile) file = NULL;

  if (is_fds (self)) {
    g_autoptr (GFile) rom_file = g_file_new_for_path (self->rom_location);
    char *original_rom;
    size_t original_size;

    if (!g_file_load_contents (rom_file, NULL, &original_rom, &original_size, NULL, error))
      return FALSE;

    g_assert (original_size == self->save_size);

    Nes::Core::Patcher patcher;
    Nes::Result result = patcher.Create (Nes::Core::Patcher::UPS,
                                         (const guchar *) original_rom,
                                         (const guchar *) self->save_data,
                                         self->save_size);

    if (NES_FAILED (result)) {
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to create FDS patcher");

      return FALSE;
    }

    std::ofstream stream (self->save_location, std::ifstream::out | std::ifstream::binary);
    result = patcher.Save (stream);
    if (NES_FAILED (result)) {
      g_set_error (error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Failed to save FDS data");

      return FALSE;
    }

    return TRUE;
  }

  if (!self->save_data)
    return TRUE;

  file = g_file_new_for_path (self->save_location);

  return g_file_replace_contents (file, self->save_data, self->save_size, NULL, FALSE,
                                  G_FILE_CREATE_NONE, NULL, NULL, error);
}

static void
nestopia_core_load_state (HsCore          *core,
                          const char      *path,
                          HsStateCallback  callback)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Machine machine (*self->emulator);
  GError *error = NULL;

  std::ifstream state_file (path, std::ifstream::in | std::ifstream::binary);

  if (!state_file.is_open ()) {
    g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_IO, "Failed to open state file");
    callback (core, &error);
    return;
  }

  Nes::Result result = machine.LoadState (state_file);
  state_file.close();

  if (NES_FAILED (result)) {
    switch (result) {
    case Nes::RESULT_ERR_NOT_READY:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Not ready to load state");
      break;
    case Nes::RESULT_ERR_INVALID_CRC:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Invalid CRC");
      break;
    case Nes::RESULT_ERR_OUT_OF_MEMORY:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_OUT_OF_MEMORY, "Out of memory");
      break;
    default:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Unknown error: %d", result);
      break;
    }

    callback (core, &error);
    return;
  }

  callback (core, NULL);
}

static void
nestopia_core_save_state (HsCore          *core,
                          const char      *path,
                          HsStateCallback  callback)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Machine machine (*self->emulator);
  GError *error = NULL;

  std::ofstream state_file (path, std::ifstream::out | std::ifstream::binary);

  if (!state_file.is_open ()) {
    g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_IO, "Failed to open state file");

    callback (core, &error);
    return;
  }

  Nes::Result result = machine.SaveState (state_file, Nes::Api::Machine::NO_COMPRESSION);
  state_file.close ();

  if (NES_FAILED (result)) {
    switch (result) {
    case Nes::RESULT_ERR_NOT_READY:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Not ready to save state");
      break;
    case Nes::RESULT_ERR_OUT_OF_MEMORY:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_OUT_OF_MEMORY, "Out of memory");
      break;
    default:
      g_set_error (&error, HS_CORE_ERROR, HS_CORE_ERROR_INTERNAL, "Unknown error: %d", result);
      break;
    }

    callback (core, &error);
    return;
  }

  callback (core, NULL);
}

static double
nestopia_core_get_frame_rate (HsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  if (is_pal (self))
    return Nes::Api::Machine::CLK_PAL_DOT / Nes::Api::Machine::CLK_PAL_VSYNC;
  else
    return Nes::Api::Machine::CLK_NTSC_DOT / Nes::Api::Machine::CLK_NTSC_VSYNC;
}

static double
nestopia_core_get_aspect_ratio (HsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  double multiplier, width, height;

  width = Nes::Api::Video::Output::WIDTH - HORZ_OVERSCAN * 2;
  height = Nes::Api::Video::Output::HEIGHT - VERT_OVERSCAN * 2;

  if (is_pal (self))
    multiplier = 2950000.0 / 2128137.0;
  else
    multiplier = 8.0 / 7.0;

  return width * multiplier / height;
}

static double
nestopia_core_get_sample_rate (HsCore *core)
{
  return SAMPLE_RATE;
}

static int
nestopia_core_get_channels (HsCore *core)
{
  return 1;
}

static HsRegion
nestopia_core_get_region (HsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  return is_pal (self) ? HS_REGION_PAL : HS_REGION_NTSC;
}

static void
nestopia_core_finalize (GObject *object)
{
  NestopiaCore *self = NESTOPIA_CORE (object);

  delete self->emulator;
  delete self->video;
  delete self->input;

  G_OBJECT_CLASS (nestopia_core_parent_class)->finalize (object);
}

static void
nestopia_core_class_init (NestopiaCoreClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  HsCoreClass *core_class = HS_CORE_CLASS (klass);

  object_class->finalize = nestopia_core_finalize;

  core_class->load_rom = nestopia_core_load_rom;
  core_class->poll_input = nestopia_core_poll_input;
  core_class->run_frame = nestopia_core_run_frame;
  core_class->reset = nestopia_core_reset;
  core_class->stop = nestopia_core_stop;

  core_class->reload_save = nestopia_core_reload_save;
  core_class->sync_save = nestopia_core_sync_save;

  core_class->load_state = nestopia_core_load_state;
  core_class->save_state = nestopia_core_save_state;

  core_class->get_frame_rate = nestopia_core_get_frame_rate;
  core_class->get_aspect_ratio = nestopia_core_get_aspect_ratio;

  core_class->get_sample_rate = nestopia_core_get_sample_rate;
  core_class->get_channels = nestopia_core_get_channels;

  core_class->get_region = nestopia_core_get_region;
}

static void
nestopia_core_init (NestopiaCore *self)
{
  self->emulator = new Nes::Api::Emulator;
  self->video = new Nes::Core::Video::Output;
  self->sound = new Nes::Core::Sound::Output;
  self->input = new Nes::Core::Input::Controllers;

  Nes::Core::Video::Output::lockCallback.Set (video_lock_cb, self);
  Nes::Core::Video::Output::unlockCallback.Set (video_unlock_cb, self);
  Nes::Api::User::fileIoCallback.Set (file_io_cb, self);
  Nes::Api::User::logCallback.Set (log_cb, self);
}

static guint
nestopia_nes_core_get_players (HsNesCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Input input (*self->emulator);

  Nes::Api::Input::Type port2_type = input.GetConnectedController (Nes::Api::Input::PORT_2);
  Nes::Api::Input::Type port3_type = input.GetConnectedController (Nes::Api::Input::PORT_3);
  Nes::Api::Input::Type port4_type = input.GetConnectedController (Nes::Api::Input::PORT_4);

  if (port4_type == Nes::Api::Input::PAD4)
    return 4;

  if (port3_type == Nes::Api::Input::PAD3)
    return 3;

  if (port2_type == Nes::Api::Input::PAD2)
    return 2;

  return 1;
}

static gboolean
nestopia_nes_core_get_has_mic (HsNesCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);

  if (hs_core_get_platform (HS_CORE (self)) == HS_PLATFORM_FAMICOM_DISK_SYSTEM)
    return TRUE;

  const Nes::Api::Cartridge::Profile *profile = Nes::Api::Cartridge (*self->emulator).GetProfile ();

  if (profile == NULL)
    return FALSE;

  return profile->system.type == Nes::Api::Cartridge::Profile::System::FAMICOM;
}

static HsNesAccessory
nestopia_nes_core_get_accessory (HsNesCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Input input (*self->emulator);

  Nes::Api::Input::Type type = input.GetConnectedController (Nes::Api::Input::EXPANSION_PORT);

  Nes::Api::Input::Type port2_type = input.GetConnectedController (Nes::Api::Input::PORT_2);
  if (port2_type != Nes::Api::Input::UNCONNECTED && port2_type != Nes::Api::Input::PAD2)
    type = port2_type;

  switch (type) {
  case Nes::Api::Input::ZAPPER:
    return HS_NES_ACCESSORY_ZAPPER;
  case Nes::Api::Input::PADDLE:
    return HS_NES_ACCESSORY_PADDLE;
  default:
    return HS_NES_ACCESSORY_NONE;
  }
}

static void
nestopia_nes_core_init (HsNesCoreInterface *iface)
{
  iface->get_players = nestopia_nes_core_get_players;
  iface->get_has_mic = nestopia_nes_core_get_has_mic;
  iface->get_accessory = nestopia_nes_core_get_accessory;
}

static void
nestopia_fds_core_set_bios_path (HsFdsCore  *core,
                                 const char *path)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Fds fds (*self->emulator);
  std::ifstream bios_file (path, std::ios::in | std::ios::binary);

  fds.SetBIOS (&bios_file);
}

static guint
nestopia_fds_core_get_n_sides (HsFdsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Fds fds (*self->emulator);

  return fds.GetNumSides ();
}

static guint
nestopia_fds_core_get_side (HsFdsCore *core)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Fds fds (*self->emulator);

  return fds.GetCurrentDisk () * 2 + fds.GetCurrentDiskSide ();
}

static void
nestopia_fds_core_set_side (HsFdsCore *core,
                            guint      side)
{
  NestopiaCore *self = NESTOPIA_CORE (core);
  Nes::Api::Fds fds (*self->emulator);

  fds.EjectDisk ();
  fds.InsertDisk (side / 2, side % 2);
}

static void
nestopia_fds_core_init (HsFdsCoreInterface *iface)
{
  iface->set_bios_path = nestopia_fds_core_set_bios_path;
  iface->get_n_sides = nestopia_fds_core_get_n_sides;
  iface->get_side = nestopia_fds_core_get_side;
  iface->set_side = nestopia_fds_core_set_side;
}

GType
hs_get_core_type (void)
{
  return NESTOPIA_TYPE_CORE;
}
